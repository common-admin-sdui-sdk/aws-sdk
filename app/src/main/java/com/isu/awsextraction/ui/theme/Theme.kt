package com.isu.awsextraction.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat
import com.isu.awssdk.ui.theme.DarkError
import com.isu.awssdk.ui.theme.DarkPrimary
import com.isu.awssdk.ui.theme.DarkSecondary
import com.isu.awssdk.ui.theme.DarkTertiary
import com.isu.awssdk.ui.theme.LightError
import com.isu.awssdk.ui.theme.LightPrimary
import com.isu.awssdk.ui.theme.LightSecondary
import com.isu.awssdk.ui.theme.LightTertiary

val DarkColorScheme = darkColorScheme(
    primary = DarkPrimary,
    onPrimary = Color.White,
    secondary = DarkSecondary,
    onSecondary = Color.White,
    tertiary = DarkTertiary,
    onTertiary = Color.White,
    error = DarkError,
    onError = Color.White,
    /*background = Grey10,
    onBackground = Grey90,
    surface = Grey10,
    onSurface = Grey80*/
)

val LightColorScheme = lightColorScheme(
    primary = LightPrimary,
    onPrimary = Color.White,
    secondary = LightSecondary,
    onSecondary = Color.White,
    tertiary = LightTertiary,
    onTertiary = Color.White,
    error = LightError,
    onError = Color.White,
    /*background = Grey10,
    onBackground = Grey90,
    surface = Grey10,
    onSurface = Grey80*/
)

@Composable
fun AwsExtractionSdkTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    dynamicColor: Boolean = true,
    content: @Composable () -> Unit
) {
    val colorScheme = when {
        dynamicColor && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (darkTheme) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        darkTheme -> DarkColorScheme
        else -> LightColorScheme
    }
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = darkTheme
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}