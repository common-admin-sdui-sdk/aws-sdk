package com.isu.awsextraction

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.isu.awsextraction.ui.theme.AwsExtractionSdkTheme
import com.isu.awssdk.core.AwsSdkConstants
import com.isu.awssdk.ui.AwsSdkActivity
import com.isu.awssdk.ui.composables.CustomButton

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AwsExtractionSdkTheme(
                dynamicColor = false
            ) {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {

                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.SpaceEvenly,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        CustomButton(
                            modifier = Modifier.fillMaxWidth(),
                            buttonText = "Extract Aadhaar Front"
                        ) {
                            val intent = Intent(applicationContext, AwsSdkActivity::class.java)
                            intent.putExtra("imageType", AwsSdkConstants.AADHAAR_CARD_FRONT)
                            startActivity(intent)
                        }

                        CustomButton(
                            modifier = Modifier.fillMaxWidth(),
                            buttonText = "Extract Aadhaar Back"
                        ) {
                            val intent = Intent(applicationContext, AwsSdkActivity::class.java)
                            intent.putExtra("imageType", AwsSdkConstants.AADHAAR_CARD_BACK)
                            startActivity(intent)
                        }

                        CustomButton(
                            modifier = Modifier.fillMaxWidth(),
                            buttonText = "Extract PAN"
                        ) {
                            val intent = Intent(applicationContext, AwsSdkActivity::class.java)
                            intent.putExtra("imageType", AwsSdkConstants.PAN_CARD)
                            startActivity(intent)
                        }

                        CustomButton(
                            modifier = Modifier.fillMaxWidth(),
                            buttonText = "Face Detection"
                        ) {
                            val intent = Intent(applicationContext, AwsSdkActivity::class.java)
                            intent.putExtra("imageType", AwsSdkConstants.FACE_LIVELINESS)
                            startActivity(intent)
                        }
                    }
                }
            }
        }
    }
}