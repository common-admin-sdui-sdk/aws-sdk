package com.isu.awsextraction

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AwsApplication: Application() {

    override fun onCreate() {
        super.onCreate()


    }
}