package com.isu.awssdk.domain.usecase

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.response.AadhaarBackExtractionResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow

class AadhaarBackExtractionUseCase constructor(private val extractionRepository: ExtractionRepository) {

    operator fun invoke(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<AadhaarBackExtractionResponseDto>> {
        return extractionRepository.aadhaarBackExtraction(
            extractionRequestDto = extractionRequestDto
        )
    }
}