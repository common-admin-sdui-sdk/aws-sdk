package com.isu.awssdk.domain.usecase

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.request.CompareRequestDto
import com.isu.awssdk.data.remote.response.CompareResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow

class SelfieAadhaarCompareUseCase constructor(private val extractionRepository: ExtractionRepository) {

    operator fun invoke(
        compareRequestDto: CompareRequestDto
    ): Flow<NetworkResult<CompareResponseDto>> {
        return extractionRepository.compareSelfieAadhaar(
            compareRequestDto = compareRequestDto
        )
    }
}