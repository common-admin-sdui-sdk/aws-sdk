package com.isu.awssdk.domain.usecase

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.response.PanExtractionResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow

class PanExtractionUseCase constructor(private val extractionRepository: ExtractionRepository) {

    operator fun invoke(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<PanExtractionResponseDto>> {
        return extractionRepository.panExtraction(
            extractionRequestDto = extractionRequestDto
        )
    }
}