package com.isu.awssdk.domain.usecase

data class AwsSdkUseCase(
    val createSessionUseCase: CreateSessionUseCase,
    val faceLivelinessDataUseCase: FaceLivelinessDataUseCase,
    val selfieAadhaarCompareUseCase: SelfieAadhaarCompareUseCase,
    val aadhaarFrontExtractionUseCase: AadhaarFrontExtractionUseCase,
    val aadhaarBackExtractionUseCase: AadhaarBackExtractionUseCase,
    val panExtractionUseCase: PanExtractionUseCase
)
