package com.isu.awssdk.domain.repository

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.request.CompareRequestDto
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.request.FaceLivelinessRequestDto
import com.isu.awssdk.data.remote.response.AadhaarBackExtractionResponseDto
import com.isu.awssdk.data.remote.response.AadhaarFrontExtractionResponseDto
import com.isu.awssdk.data.remote.response.CompareResponseDto
import com.isu.awssdk.data.remote.response.CreateSessionIdResponseDto
import com.isu.awssdk.data.remote.response.FaceLivelinessResponseDto
import com.isu.awssdk.data.remote.response.PanExtractionResponseDto
import kotlinx.coroutines.flow.Flow

interface ExtractionRepository {

    fun createSession(): Flow<NetworkResult<CreateSessionIdResponseDto>>

    fun fetchFaceLivelinessData(
        faceLivelinessRequestDto: FaceLivelinessRequestDto
    ): Flow<NetworkResult<FaceLivelinessResponseDto>>

    fun compareSelfieAadhaar(
        compareRequestDto: CompareRequestDto
    ): Flow<NetworkResult<CompareResponseDto>>

    fun aadhaarFrontExtraction(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<AadhaarFrontExtractionResponseDto>>

    fun aadhaarBackExtraction(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<AadhaarBackExtractionResponseDto>>

    fun panExtraction(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<PanExtractionResponseDto>>
}