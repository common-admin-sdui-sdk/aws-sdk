package com.isu.awssdk.domain.usecase

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.response.CreateSessionIdResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow

class CreateSessionUseCase constructor(private val extractionRepository: ExtractionRepository) {

    operator fun invoke(): Flow<NetworkResult<CreateSessionIdResponseDto>> {
        return extractionRepository.createSession()
    }
}