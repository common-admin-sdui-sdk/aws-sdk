package com.isu.awssdk.domain.usecase

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.response.AadhaarFrontExtractionResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow

class AadhaarFrontExtractionUseCase constructor(private val extractionRepository: ExtractionRepository) {

    operator fun invoke(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<AadhaarFrontExtractionResponseDto>> {
        return extractionRepository.aadhaarFrontExtraction(
            extractionRequestDto = extractionRequestDto
        )
    }
}