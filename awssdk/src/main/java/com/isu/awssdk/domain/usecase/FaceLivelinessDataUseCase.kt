package com.isu.awssdk.domain.usecase

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.data.remote.request.FaceLivelinessRequestDto
import com.isu.awssdk.data.remote.response.FaceLivelinessResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow

class FaceLivelinessDataUseCase constructor(private val extractionRepository: ExtractionRepository) {

    operator fun invoke(
        faceLivelinessRequestDto: FaceLivelinessRequestDto
    ): Flow<NetworkResult<FaceLivelinessResponseDto>> {
        return extractionRepository.fetchFaceLivelinessData(
            faceLivelinessRequestDto = faceLivelinessRequestDto
        )
    }
}