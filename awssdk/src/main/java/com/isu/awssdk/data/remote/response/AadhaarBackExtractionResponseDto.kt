package com.isu.awssdk.data.remote.response

import com.google.gson.annotations.SerializedName

data class AadhaarBackExtractionResponseDto(
    @field:SerializedName("code")
    val code: String,

    @field:SerializedName("devMsg")
    val devMsg: String,

    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("status")
    val status: Int,

    @field:SerializedName("data")
    val data: DataEntity
) {
    data class DataEntity(
        @field:SerializedName("aadhaarNumber")
        val aadhaarNumber: String,

        @field:SerializedName("address")
        val address: String,

        @field:SerializedName("city")
        val city: String,

        @field:SerializedName("confidence")
        val confidence: Double,

        @field:SerializedName("details")
        val details: DetailsEntity,

        @field:SerializedName("district")
        val district: String,

        @field:SerializedName("idType")
        val idType: String,

        @field:SerializedName("og_add")
        val ogAdd: String,

        @field:SerializedName("pinCode")
        val pinCode: String,

        @field:SerializedName("state")
        val state: String,

        @field:SerializedName("validate")
        val validate: Boolean
    ) {
        data class DetailsEntity(
            @field:SerializedName("aadhaarNumber")
            val aadhaarNumber: AadhaarNumberEntity,

            @field:SerializedName("address")
            val address: AddressEntity
        ) {
            data class AadhaarNumberEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("isVerified")
                val isVerified: Boolean,

                @field:SerializedName("text")
                val text: String
            )

            data class AddressEntity(
                @field:SerializedName("address")
                val address: String,

                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("district")
                val district: String,

                @field:SerializedName("pincode")
                val pinCode: String,

                @field:SerializedName("state")
                val state: String
            )
        }
    }
}
