package com.isu.awssdk.data.model

data class PanDetails(
    var panNumber: String = "",
    var panName: String = "",
    var panFatherName: String = "",
    var panDob: String = "",
    var panImageUrl: String = ""
)
