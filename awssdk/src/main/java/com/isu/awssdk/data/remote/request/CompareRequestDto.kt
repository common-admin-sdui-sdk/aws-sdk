package com.isu.awssdk.data.remote.request

import com.google.gson.annotations.SerializedName

data class CompareRequestDto(
    @field:SerializedName("selfie")
    val selfie: String,
    @field:SerializedName("aadhar")
    val aadhaar: String
)
