package com.isu.awssdk.data.model

data class AadhaarBackDetails(
    var aadhaarNumber: String = "",
    var aadhaarAddress: String = "",
    var aadhaarCity: String = "",
    var aadhaarState: String = "",
    var aadhaarDistrict: String = "",
    var aadhaarPin: String = "",
    var aadhaarOgAddress: String = "",
    var aadhaarBackImageUrl: String = ""
)
