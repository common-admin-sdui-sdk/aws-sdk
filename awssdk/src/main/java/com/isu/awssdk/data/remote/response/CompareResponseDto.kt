package com.isu.awssdk.data.remote.response

import com.google.gson.annotations.SerializedName

data class CompareResponseDto(
   @field:SerializedName("status")
    val status: String?,

    @field:SerializedName("similarity")
    val similarity: String?
)
