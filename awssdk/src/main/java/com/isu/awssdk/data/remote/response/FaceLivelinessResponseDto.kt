package com.isu.awssdk.data.remote.response

import com.google.gson.annotations.SerializedName

data class FaceLivelinessResponseDto(
    @field:SerializedName("status")
    val status: String?,

    @field:SerializedName("confidence")
    val confidence: String?,

    @field:SerializedName("b64string")
    val base64String: String?
)
