package com.isu.awssdk.data.remote.request

import com.google.gson.annotations.SerializedName

data class ExtractionRequestDto(
    @field:SerializedName("image")
    val image: String
)
