package com.isu.awssdk.data.remote.response

import com.google.gson.annotations.SerializedName

data class CreateSessionIdResponseDto(
    @field:SerializedName("sessionid")
    val sessionId: String?
)