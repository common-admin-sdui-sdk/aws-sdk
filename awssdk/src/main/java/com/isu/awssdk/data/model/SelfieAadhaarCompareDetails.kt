package com.isu.awssdk.data.model

data class SelfieAadhaarCompareDetails(
    var similarity: String = ""
)
