package com.isu.awssdk.data.repository

import com.isu.awssdk.core.NetworkResult
import com.isu.awssdk.core.NetworkResultHandler
import com.isu.awssdk.data.remote.AwsSdkApiService
import com.isu.awssdk.data.remote.request.CompareRequestDto
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.request.FaceLivelinessRequestDto
import com.isu.awssdk.data.remote.response.AadhaarBackExtractionResponseDto
import com.isu.awssdk.data.remote.response.AadhaarFrontExtractionResponseDto
import com.isu.awssdk.data.remote.response.CompareResponseDto
import com.isu.awssdk.data.remote.response.CreateSessionIdResponseDto
import com.isu.awssdk.data.remote.response.FaceLivelinessResponseDto
import com.isu.awssdk.data.remote.response.PanExtractionResponseDto
import com.isu.awssdk.domain.repository.ExtractionRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ExtractionRepositoryImpl @Inject constructor(
    private val awsSdkApiService: AwsSdkApiService
) : ExtractionRepository {

    override fun createSession(): Flow<NetworkResult<CreateSessionIdResponseDto>> {
        return NetworkResultHandler.handleResponse {
            awsSdkApiService.createSession()
        }
    }

    override fun fetchFaceLivelinessData(
        faceLivelinessRequestDto: FaceLivelinessRequestDto
    ): Flow<NetworkResult<FaceLivelinessResponseDto>> {
        return NetworkResultHandler.handleResponse {
            awsSdkApiService.fetchFaceLivelinessData(
                faceLivelinessRequestDto = faceLivelinessRequestDto
            )
        }
    }

    override fun compareSelfieAadhaar(
        compareRequestDto: CompareRequestDto
    ): Flow<NetworkResult<CompareResponseDto>> {
        return NetworkResultHandler.handleResponse {
            awsSdkApiService.compareSelfieAadhaar(
                compareRequestDto = compareRequestDto
            )
        }
    }

    override fun aadhaarFrontExtraction(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<AadhaarFrontExtractionResponseDto>> {
        return NetworkResultHandler.handleResponse {
            awsSdkApiService.aadhaarFrontExtraction(
                extractionRequestDto = extractionRequestDto
            )
        }
    }

    override fun aadhaarBackExtraction(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<AadhaarBackExtractionResponseDto>> {
        return NetworkResultHandler.handleResponse {
            awsSdkApiService.aadhaarBackExtraction(
                extractionRequestDto = extractionRequestDto
            )
        }
    }

    override fun panExtraction(
        extractionRequestDto: ExtractionRequestDto
    ): Flow<NetworkResult<PanExtractionResponseDto>> {
        return NetworkResultHandler.handleResponse {
            awsSdkApiService.panExtraction(
                extractionRequestDto = extractionRequestDto
            )
        }
    }
}