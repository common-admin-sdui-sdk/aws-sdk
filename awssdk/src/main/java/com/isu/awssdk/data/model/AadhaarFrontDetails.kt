package com.isu.awssdk.data.model

data class AadhaarFrontDetails(
    var aadhaarName: String = "",
    var aadhaarFatherName: String = "",
    var aadhaarIssueDate: String = "",
    var aadhaarDob: String = "",
    var aadhaarGender: String = "",
    var aadhaarNumber: String = "",
    var aadhaarVidNumber: String = "",
    var aadhaarFrontImageUrl: String = ""
)
