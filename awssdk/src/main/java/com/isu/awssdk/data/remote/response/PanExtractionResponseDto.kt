package com.isu.awssdk.data.remote.response

import com.google.gson.annotations.SerializedName

data class PanExtractionResponseDto(
    @field:SerializedName("code")
    val code: String,

    @field:SerializedName("data")
    val data: DataEntity,

    @field:SerializedName("devMsg")
    val devMsg: String,

    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("status")
    val status: Int
) {
    data class DataEntity(
        @field:SerializedName("DOB")
        val dob: String,

        @field:SerializedName("confidence")
        val confidence: Double,

        @field:SerializedName("details")
        val details: DetailsEntity,

        @field:SerializedName("fatherName")
        val fatherName: String,

        @field:SerializedName("idType")
        val idType: String,

        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("pan")
        val pan: String
    ) {
        data class DetailsEntity(
            @field:SerializedName("DOB")
            val dob: DobEntity,

            @field:SerializedName("fatherName")
            val fatherName: FatherNameEntity,

            @field:SerializedName("name")
            val name: NameEntity,

            @field:SerializedName("pan")
            val pan: PanEntity
        ) {
            data class PanEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class NameEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class FatherNameEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class DobEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )
        }
    }
}
