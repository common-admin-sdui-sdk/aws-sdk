package com.isu.awssdk.data.remote.request

import com.google.gson.annotations.SerializedName

data class FaceLivelinessRequestDto(
    @field:SerializedName("sessionid")
    val sessionId: String
)
