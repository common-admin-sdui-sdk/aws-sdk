package com.isu.awssdk.data.remote.response

import com.google.gson.annotations.SerializedName

data class AadhaarFrontExtractionResponseDto(
    @field:SerializedName("code")
    val code: String,

    @field:SerializedName("devMsg")
    val devMsg: String,

    @field:SerializedName("message")
    val message: String,

    @field:SerializedName("status")
    val status: Int,

    @field:SerializedName("data")
    val data: DataEntity,
) {
    data class DataEntity(
        @field:SerializedName("DOB")
        val dob: String,

        @field:SerializedName("aadhaarNumber")
        val aadhaarNumber: String,

        @field:SerializedName("confidience")
        val confidence: Double,

        @field:SerializedName("father")
        val father: String,

        @field:SerializedName("gender")
        val gender: String,

        @field:SerializedName("idType")
        val idType: String,

        @field:SerializedName("issueDate")
        val issueDate: String,

        @field:SerializedName("name")
        val name: String,

        @field:SerializedName("validate")
        val validate: Boolean,

        @field:SerializedName("details")
        val details: DetailsEntity
    ) {
        data class DetailsEntity(
            @field:SerializedName("DOB")
            val dob: DobEntity,

            @field:SerializedName("aadhaarNumber")
            val aadhaarNumber: AadhaarNumberEntity,

            @field:SerializedName("gender")
            val gender: GenderEntity,

            @field:SerializedName("issueDate")
            val issueDate: IssueDateEntity,

            @field:SerializedName("name")
            val name: NameEntity,

            @field:SerializedName("photo")
            val photo: PhotoEntity,

            @field:SerializedName("vid")
            val vid: VidEntity
        ) {
            data class DobEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class AadhaarNumberEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("isVerified")
                val isVerified: Boolean,

                @field:SerializedName("text")
                val text: String
            )

            data class GenderEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class IssueDateEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class NameEntity(
                @field:SerializedName("confidence")
                val confidence: Double,

                @field:SerializedName("text")
                val text: String
            )

            data class PhotoEntity(
                @field:SerializedName("confidence")
                val confidence: Double
            )

            data class VidEntity(
                @field:SerializedName("confidence")
                val confidence: Int,

                @field:SerializedName("text")
                val text: String
            )
        }
    }
}
