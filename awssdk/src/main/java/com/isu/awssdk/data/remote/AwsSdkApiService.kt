package com.isu.awssdk.data.remote

import com.isu.awssdk.data.remote.request.CompareRequestDto
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.request.FaceLivelinessRequestDto
import com.isu.awssdk.data.remote.response.AadhaarBackExtractionResponseDto
import com.isu.awssdk.data.remote.response.AadhaarFrontExtractionResponseDto
import com.isu.awssdk.data.remote.response.CompareResponseDto
import com.isu.awssdk.data.remote.response.CreateSessionIdResponseDto
import com.isu.awssdk.data.remote.response.FaceLivelinessResponseDto
import com.isu.awssdk.data.remote.response.PanExtractionResponseDto
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AwsSdkApiService {

    @GET("sessioncreate")
    suspend fun createSession(): Response<CreateSessionIdResponseDto>

    @POST("sessioncreate")
    suspend fun fetchFaceLivelinessData(
        @Body faceLivelinessRequestDto: FaceLivelinessRequestDto
    ): Response<FaceLivelinessResponseDto>

    @POST("compare")
    suspend fun compareSelfieAadhaar(
        @Body compareRequestDto: CompareRequestDto
    ): Response<CompareResponseDto>

    @POST("aadhaarfrontextract")
    suspend fun aadhaarFrontExtraction(
        @Body extractionRequestDto: ExtractionRequestDto
    ): Response<AadhaarFrontExtractionResponseDto>

    @POST("aadhaarbackextract")
    suspend fun aadhaarBackExtraction(
        @Body extractionRequestDto: ExtractionRequestDto
    ): Response<AadhaarBackExtractionResponseDto>

    @POST("panextract")
    suspend fun panExtraction(
        @Body extractionRequestDto: ExtractionRequestDto
    ): Response<PanExtractionResponseDto>
}