package com.isu.awssdk.core

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import com.isu.awssdk.ui.navgraph.AwsSdkDestination
import java.io.ByteArrayOutputStream

class AwsSdkUtils {

    companion object {

        fun isForDocumentVerification(): Boolean {
            return AwsSdkConstants.INTENT_IMAGE_TYPE == AwsSdkConstants.AADHAAR_CARD_FRONT ||
                    AwsSdkConstants.INTENT_IMAGE_TYPE == AwsSdkConstants.AADHAAR_CARD_BACK ||
                    AwsSdkConstants.INTENT_IMAGE_TYPE == AwsSdkConstants.PAN_CARD
        }

        fun isForSelfieVerification(): Boolean {
            return AwsSdkConstants.INTENT_IMAGE_TYPE == AwsSdkConstants.FACE_LIVELINESS
        }

        fun checkCameraPermission(context: Context): Boolean {
            return ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) ==
                    PackageManager.PERMISSION_GRANTED
        }

        fun Context.showToast(msg: String) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }

        fun String.imageDescription(): String {
            when {
                this.equals(AwsSdkConstants.AADHAAR_CARD_FRONT, ignoreCase = false) -> {
                    return "Aadhaar Front Image"
                }

                this.equals(AwsSdkConstants.AADHAAR_CARD_BACK, ignoreCase = false) -> {
                    return "Aadhaar Back Image"
                }

                this.equals(AwsSdkConstants.PAN_CARD, ignoreCase = false) -> {
                    return "PAN Image"
                }

                else -> {
                    return ""
                }
            }
        }

        fun Uri.encodeImage(context: Context): String {
            val imageStream = context.contentResolver.openInputStream(this)
            val bitmapImage = BitmapFactory.decodeStream(imageStream)
            val byteArray = ByteArrayOutputStream()
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, byteArray)
            val byteStreamToByteArray = byteArray.toByteArray()
            return Base64.encodeToString(byteStreamToByteArray, Base64.DEFAULT)
        }

        fun AwsSdkDestination.startNavigation(navHostController: NavHostController) {
            navHostController.navigate(route = this.route) {
                popUpTo(navHostController.graph.findStartDestination().id) {
                    saveState = true
                }
                launchSingleTop = true
                restoreState = true
            }
        }
    }
}