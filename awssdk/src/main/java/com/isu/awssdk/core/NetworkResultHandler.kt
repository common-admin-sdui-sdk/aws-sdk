package com.isu.awssdk.core

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import org.json.JSONObject
import retrofit2.Response

object NetworkResultHandler {

    fun <T> handleResponse(networkCall: suspend () -> Response<T>): Flow<NetworkResult<T>> {
        return flow {
            emit(NetworkResult.Loading(isLoading = true))

            try {
                val networkResponse = networkCall.invoke()
                networkResponse.apply {
                    if (isSuccessful) {
                        body()?.let {
                            emit(NetworkResult.Success(data = it))
                        }
                    } else {
                        val error = errorBody()?.string()?.let {
                            JSONObject(it)
                        }
                        error?.let {
                            emit(NetworkResult.Error(message = handleErrorResponse(errorJson = it)))
                        }
                    }
                }
            } catch (exception: Exception) {
                Log.e("TAG", "handleResponse: ${exception.message}")
            }


            emit(NetworkResult.Loading(isLoading = false))
        }
    }

    private fun handleErrorResponse(errorJson: JSONObject): String {
        return when {
            errorJson.has(AwsSdkConstants.MESSAGE) -> {
                errorJson.getString(AwsSdkConstants.MESSAGE)
            }

            errorJson.has(AwsSdkConstants.STATUS_DESC) -> {
                errorJson.getString(AwsSdkConstants.STATUS_DESC)
            }

            else -> {
                AwsSdkConstants.UNKNOWN_STATUS_MSG
            }
        }
    }

    /**
     * handle() takes the response from use case function as NetworkResults<> with in Main Coroutine Scope
     * return the extracted response with in onLoading(), onFailure(), onSuccess().
     **/

    fun <T> handleResponseState(
        response: Flow<NetworkResult<T>>,
        onLoading: (it: Boolean) -> Unit,
        onError: (it: String) -> Unit,
        onSuccess: (it: T) -> Unit
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            response.collectLatest { networkResult ->
                when (networkResult) {
                    is NetworkResult.Error -> {
                        onError.invoke(networkResult.message!!)
                    }

                    is NetworkResult.Loading -> {
                        onLoading.invoke(networkResult.isLoading)
                    }

                    is NetworkResult.Success -> {
                        onSuccess.invoke(networkResult.data!!)
                    }
                }
            }
        }
    }
}