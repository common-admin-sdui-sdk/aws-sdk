package com.isu.awssdk.core

import android.app.Application
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.core.Amplify

class AwsSdkApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Amplify.addPlugin(AWSCognitoAuthPlugin())
        Amplify.configure(applicationContext)
    }
}