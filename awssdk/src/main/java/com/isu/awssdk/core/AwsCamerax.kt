package com.isu.awssdk.core

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCapture.OnImageSavedCallback
import androidx.camera.core.ImageCaptureException
import androidx.camera.view.LifecycleCameraController
import androidx.core.content.ContextCompat

class AwsCamerax {

    companion object {

        fun captureDocument(
            context: Context,
            controller: LifecycleCameraController,
            onImageCaptured: (Uri) -> Unit,
            onError: (ImageCaptureException) -> Unit
        ) {
            val timestamp = System.currentTimeMillis()
            val contentValues = ContentValues().apply {
                this.put(MediaStore.Images.Media.DISPLAY_NAME, timestamp)
                this.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
            }
            val outputOptions = ImageCapture.OutputFileOptions.Builder(
                context.contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            ).build()

            controller.takePicture(
                outputOptions,
                ContextCompat.getMainExecutor(context),
                object : OnImageSavedCallback {
                    override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                        val capturedImageUri = outputFileResults.savedUri
                        capturedImageUri?.let { onImageCaptured(it) }
                    }

                    override fun onError(exception: ImageCaptureException) {
                        onError(exception)
                    }
                }
            )
        }
    }
}