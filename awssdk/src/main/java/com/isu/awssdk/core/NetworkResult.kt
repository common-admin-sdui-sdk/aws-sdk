package com.isu.awssdk.core

sealed class NetworkResult<T>(
    val data: T? = null,
    val message: String? = null,
    val isLoading: Boolean = false
) {
    class Success<T>(data: T?) : NetworkResult<T>(data = data)

    class Error<T>(data: T? = null, message: String?) : NetworkResult<T>(data = data, message = message)

    class Loading<T>(isLoading: Boolean) : NetworkResult<T>(isLoading = isLoading)
}