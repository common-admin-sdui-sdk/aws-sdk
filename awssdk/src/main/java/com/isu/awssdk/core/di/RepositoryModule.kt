package com.isu.awssdk.core.di

import com.isu.awssdk.data.remote.AwsSdkApiService
import com.isu.awssdk.data.repository.ExtractionRepositoryImpl
import com.isu.awssdk.domain.repository.ExtractionRepository
import com.isu.awssdk.domain.usecase.AadhaarBackExtractionUseCase
import com.isu.awssdk.domain.usecase.AadhaarFrontExtractionUseCase
import com.isu.awssdk.domain.usecase.AwsSdkUseCase
import com.isu.awssdk.domain.usecase.CreateSessionUseCase
import com.isu.awssdk.domain.usecase.FaceLivelinessDataUseCase
import com.isu.awssdk.domain.usecase.PanExtractionUseCase
import com.isu.awssdk.domain.usecase.SelfieAadhaarCompareUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun providesExtractionRepository(
        awsSdkApiService: AwsSdkApiService
    ): ExtractionRepository {
        return ExtractionRepositoryImpl(
            awsSdkApiService = awsSdkApiService
        )
    }

    @Provides
    @Singleton
    fun providesAwsSdkUseCases(
        extractionRepository: ExtractionRepository
    ): AwsSdkUseCase {
        return AwsSdkUseCase(
            createSessionUseCase = CreateSessionUseCase(extractionRepository = extractionRepository),
            faceLivelinessDataUseCase = FaceLivelinessDataUseCase(extractionRepository = extractionRepository),
            selfieAadhaarCompareUseCase = SelfieAadhaarCompareUseCase(extractionRepository = extractionRepository),
            aadhaarFrontExtractionUseCase = AadhaarFrontExtractionUseCase(extractionRepository = extractionRepository),
            aadhaarBackExtractionUseCase = AadhaarBackExtractionUseCase(extractionRepository = extractionRepository),
            panExtractionUseCase = PanExtractionUseCase(extractionRepository = extractionRepository)
        )
    }
}