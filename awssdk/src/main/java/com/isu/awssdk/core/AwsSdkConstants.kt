package com.isu.awssdk.core

object AwsSdkConstants {

    val MOBILE = ""
    val ADMIN_NAME = ""
    val USER_NAME = ""
    var INTENT_IMAGE_TYPE = ""

    const val DOC_CAPTURE = "documentCapture"
    const val INSTRUCTION = "instruction"
    const val DOC_REVIEW = "documentReview"
    const val SELFIE_CAPTURE = "selfieCapture"

    const val BASE_URL = "https://ocr.iserveu.tech/"

    const val MESSAGE = "message"
    const val STATUS_DESC = "statusDesc"
    const val UNKNOWN_STATUS_MSG = "Something went wrong, please try after some time."

    const val AADHAAR_CARD_FRONT = "aadhaarCardFront"
    const val AADHAAR_CARD_BACK = "aadhaarCardBack"
    const val PAN_CARD = "panCard"
    const val FACE_LIVELINESS = "faceLiveliness"
    const val IMAGE_TYPE = "imageType"

    var sessionId = ""
}