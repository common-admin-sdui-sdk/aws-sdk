package com.isu.awssdk.ui.theme

import androidx.compose.ui.graphics.Color


val LightPrimary = Color(0xFF260161)
val LightSecondary = Color(0xFF380191)
val LightTertiary = Color(0xFF3C6101)
val LightError = Color(0xFF630303)

val DarkPrimary = Color(0xFFD4CCDF)
val DarkSecondary = Color(0xFFD7CCE9)
val DarkTertiary = Color(0xFFD8DFCC)
val DarkError = Color(0xFFE0CDCD)