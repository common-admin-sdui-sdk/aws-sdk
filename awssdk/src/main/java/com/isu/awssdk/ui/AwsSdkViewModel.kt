package com.isu.awssdk.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import com.isu.awssdk.core.AwsSdkConstants
import com.isu.awssdk.core.AwsSdkUtils.Companion.encodeImage
import com.isu.awssdk.core.AwsSdkUtils.Companion.showToast
import com.isu.awssdk.core.AwsSdkUtils.Companion.startNavigation
import com.isu.awssdk.core.NetworkResultHandler.handleResponseState
import com.isu.awssdk.data.model.AadhaarBackDetails
import com.isu.awssdk.data.model.AadhaarFrontDetails
import com.isu.awssdk.data.model.PanDetails
import com.isu.awssdk.data.model.SelfieAadhaarCompareDetails
import com.isu.awssdk.data.remote.request.CompareRequestDto
import com.isu.awssdk.data.remote.request.ExtractionRequestDto
import com.isu.awssdk.data.remote.request.FaceLivelinessRequestDto
import com.isu.awssdk.domain.usecase.AwsSdkUseCase
import com.isu.awssdk.ui.navgraph.AwsSdkDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AwsSdkViewModel @Inject constructor(
    private val awsSdkUseCase: AwsSdkUseCase
) : ViewModel() {

    val showInternetDialog: MutableState<Boolean> = mutableStateOf(false)
    val showSettingsDialog: MutableState<Boolean> = mutableStateOf(false)
    val aadhaarFrontUri: MutableState<Uri> = mutableStateOf(Uri.EMPTY)
    val aadhaarBackUri: MutableState<Uri> = mutableStateOf(Uri.EMPTY)
    val panImageUri: MutableState<Uri> = mutableStateOf(Uri.EMPTY)
    val reviewImage: MutableState<Uri> = mutableStateOf(Uri.EMPTY)
    val sessionId: MutableState<String> = mutableStateOf("")
    val livelinessStatus: MutableState<String> = mutableStateOf("")
    val livelinessConfidence: MutableState<String> = mutableStateOf("")
    val livelinessImage: MutableState<String> = mutableStateOf("")
    val showLoader: MutableState<Boolean> = mutableStateOf(false)
    val selfieAadhaarCompareDetails: MutableState<SelfieAadhaarCompareDetails> =
        mutableStateOf(SelfieAadhaarCompareDetails())
    val aadhaarFrontDetails: MutableState<AadhaarFrontDetails> =
        mutableStateOf(AadhaarFrontDetails())
    val aadhaarBackDetails: MutableState<AadhaarBackDetails> =
        mutableStateOf(AadhaarBackDetails())
    val panDetails: MutableState<PanDetails> =
        mutableStateOf(PanDetails())

    /**
     * Create session API call
     */
    fun createSession(
        context: Context,
        navHostController: NavHostController
    ) {
        val createSessionResponse = awsSdkUseCase.createSessionUseCase.invoke()

        handleResponseState(
            response = createSessionResponse,
            onLoading = {
                showLoader.value = it
            },
            onSuccess = { createSessionRes ->
                sessionId.value = createSessionRes.sessionId ?: ""
                if (sessionId.value.isNotEmpty()) {
                    AwsSdkConstants.sessionId = sessionId.value
                    AwsSdkDestination.SelfieCapture.startNavigation(navHostController = navHostController)
                } else {
                    context.showToast("Something went wrong. Please try after sometime.")
                    navHostController.popBackStack()
                }
            },
            onError = {
                context.showToast(msg = it)
            }
        )
    }

    /**
     * PAN image extraction API call
     */
    fun startFaceLivelinessSession(
        context: Context,
        navHostController: NavHostController
    ) {
        val fetchSelfieResponse = awsSdkUseCase.faceLivelinessDataUseCase.invoke(
            faceLivelinessRequestDto = FaceLivelinessRequestDto(
                sessionId = sessionId.value
            )
        )

        handleResponseState(
            response = fetchSelfieResponse,
            onLoading = {
                showLoader.value = it
            },
            onSuccess = { selfieRes ->
                livelinessStatus.value = selfieRes.status ?: ""
                livelinessConfidence.value = selfieRes.confidence ?: ""
                livelinessImage.value = selfieRes.base64String ?: ""

                startSelfieAadhaarImageComparison(
                    context = context,
                    navHostController = navHostController
                )
            },
            onError = {
                context.showToast(msg = it)
            }
        )
    }

    /**
     * Compare image from selfie and aadhaar extraction API call
     */
    private fun startSelfieAadhaarImageComparison(
        context: Context,
        navHostController: NavHostController
    ) {
        val selfieAadhaarCompareResponse = awsSdkUseCase.selfieAadhaarCompareUseCase.invoke(
            compareRequestDto = CompareRequestDto(
                selfie = livelinessImage.value,
                aadhaar = aadhaarFrontUri.value.encodeImage(context = context)
            )
        )

        handleResponseState(
            response = selfieAadhaarCompareResponse,
            onLoading = {
                showLoader.value = it
            },
            onSuccess = { compareRes ->
                selfieAadhaarCompareDetails.value.similarity = compareRes.similarity ?: ""
                navHostController.popBackStack()
            },
            onError = {
                context.showToast(msg = it)
            }
        )
    }

    /**
     * PAN image extraction API call
     */
    fun startPanExtraction(
        context: Context
    ) {
        val panExtractionResponse = awsSdkUseCase.panExtractionUseCase.invoke(
            extractionRequestDto = ExtractionRequestDto(
                image = panImageUri.value.toString()
            )
        )

        handleResponseState(
            response = panExtractionResponse,
            onLoading = {
                showLoader.value = it
            },
            onSuccess = { panRes ->
                if (panRes.status == 0) {
                    panDetails.value.apply {
                        panNumber = panRes.data.pan
                        panName = panRes.data.name
                        panFatherName = panRes.data.fatherName
                        panDob = panRes.data.dob
                        panImageUrl = panImageUri.value.toString()
                    }
                }
            },
            onError = {
                context.showToast(msg = it)
            }
        )
    }

    /**
     * Aadhaar front image extraction API call
     */
    fun startAadhaarFrontExtraction(
        context: Context
    ) {
        val aadhaarFrontExtractionResponse = awsSdkUseCase.aadhaarFrontExtractionUseCase.invoke(
            extractionRequestDto = ExtractionRequestDto(
                image = aadhaarFrontUri.value.toString()
            )
        )

        handleResponseState(
            response = aadhaarFrontExtractionResponse,
            onLoading = {
                showLoader.value = it
            },
            onSuccess = { aadhaarFrontRes ->
                if (aadhaarFrontRes.status == 0) {
                    aadhaarFrontDetails.value.apply {
                        aadhaarName = aadhaarFrontRes.data.name
                        aadhaarFatherName = aadhaarFrontRes.data.father
                        aadhaarGender = aadhaarFrontRes.data.gender
                        aadhaarDob = aadhaarFrontRes.data.dob
                        aadhaarIssueDate = aadhaarFrontRes.data.issueDate
                        aadhaarNumber = aadhaarFrontRes.data.aadhaarNumber
                        aadhaarVidNumber = aadhaarFrontRes.data.details.vid.text
                        aadhaarFrontImageUrl = aadhaarFrontUri.value.toString()
                    }
                }
            },
            onError = {
                context.showToast(msg = it)
            }
        )
    }

    /**
     * Aadhaar back image extraction API call
     */
    fun startAadhaarBackExtraction(
        context: Context
    ) {
        val aadhaarBackExtractionResponse = awsSdkUseCase.aadhaarBackExtractionUseCase.invoke(
            extractionRequestDto = ExtractionRequestDto(
                image = aadhaarBackUri.value.toString()
            )
        )

        handleResponseState(
            response = aadhaarBackExtractionResponse,
            onLoading = {
                showLoader.value = it
            },
            onSuccess = { aadhaarBackRes ->
                if (aadhaarBackRes.status == 0) {
                    aadhaarBackDetails.value.apply {
                        aadhaarNumber = aadhaarBackRes.data.aadhaarNumber
                        aadhaarAddress = aadhaarBackRes.data.address
                        aadhaarCity = aadhaarBackRes.data.city
                        aadhaarDistrict = aadhaarBackRes.data.district
                        aadhaarPin = aadhaarBackRes.data.pinCode
                        aadhaarState = aadhaarBackRes.data.state
                        aadhaarOgAddress = aadhaarBackRes.data.ogAdd
                        aadhaarBackImageUrl = aadhaarBackUri.value.toString()
                    }
                }
            },
            onError = {
                context.showToast(msg = it)
            }
        )
    }

    fun storeCardImage(image: Uri) {
        when {
            AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.AADHAAR_CARD_FRONT, ignoreCase = false) -> {
                aadhaarFrontUri.value = image
            }

            AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.AADHAAR_CARD_BACK, ignoreCase = false) -> {
                aadhaarBackUri.value = image
            }

            AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.PAN_CARD, ignoreCase = false) -> {
                panImageUri.value = image
            }

            else -> {
                // do nothing
            }
        }
    }

    fun getCardImage() {
        when {
            AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.AADHAAR_CARD_FRONT, ignoreCase = false) -> {
                reviewImage.value = aadhaarFrontUri.value
            }

            AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.AADHAAR_CARD_BACK, ignoreCase = false) -> {
                reviewImage.value = aadhaarBackUri.value
            }

            AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.PAN_CARD, ignoreCase = false) -> {
                reviewImage.value = panImageUri.value
            }

            else -> {
                reviewImage.value = Uri.EMPTY
            }
        }
    }
}