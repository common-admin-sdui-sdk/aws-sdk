package com.isu.awssdk.ui

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SignalWifiConnectedNoInternet4
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.core.view.WindowCompat
import androidx.navigation.compose.rememberNavController
import com.amplifyframework.auth.cognito.AWSCognitoAuthPlugin
import com.amplifyframework.core.Amplify
import com.isu.awssdk.R
import com.isu.awssdk.core.AwsSdkConstants
import com.isu.awssdk.core.AwsSdkUtils
import com.isu.awssdk.core.NetworkConnection
import com.isu.awssdk.ui.composables.CustomAlertDialog
import com.isu.awssdk.ui.composables.CustomDialog
import com.isu.awssdk.ui.navgraph.AwsSdkNavHost
import com.isu.awssdk.ui.theme.AwsExtractionTheme
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AwsSdkActivity : ComponentActivity() {

    private val startNavigation: MutableState<Boolean> = mutableStateOf(false)
    private val awsSdkViewModel: AwsSdkViewModel by viewModels()
    private val countState: MutableState<Int> = mutableIntStateOf(0)
    private val permissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            handlePermissionResult(isGranted = isGranted)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        WindowCompat.setDecorFitsSystemWindows(window, false)
        Amplify.addPlugin(AWSCognitoAuthPlugin())
        Amplify.configure(applicationContext)

        intent?.let {
            startNavigation.value = it.hasExtra(AwsSdkConstants.IMAGE_TYPE)
            AwsSdkConstants.INTENT_IMAGE_TYPE = it.getStringExtra(AwsSdkConstants.IMAGE_TYPE).toString()
        }

        setContent {
            AwsExtractionTheme(
                dynamicColor = false
            ) {
                val navController = rememberNavController()

                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    if (AwsSdkUtils.checkCameraPermission(context = applicationContext)) {
                        if (NetworkConnection.isNetworkAvailable(context = applicationContext)) {
                            startNavigation.value = true
                        } else {
                            awsSdkViewModel.showInternetDialog.value = true
                        }
                    } else {
                        permissionLauncher.launch(Manifest.permission.CAMERA)
                    }

                    if (startNavigation.value) {
                        AwsSdkNavHost(
                            navHostController = navController,
                            awsSdkViewModel = awsSdkViewModel
                        )
                    }

                    if (awsSdkViewModel.showInternetDialog.value) {
                        CustomDialog(
                            warningDesc = stringResource(id = R.string.no_internet),
                            buttonText = stringResource(id = R.string.try_again),
                            imageVector = Icons.Default.SignalWifiConnectedNoInternet4,
                            setShowDialog = {
                                awsSdkViewModel.showInternetDialog.value = it
                            }
                        )
                    }

                    if (awsSdkViewModel.showSettingsDialog.value) {
                        CustomAlertDialog(
                            message = stringResource(id = R.string.alert),
                            onDismiss = {
                                awsSdkViewModel.showSettingsDialog.value = false
                                finishAffinity()
                            },
                            onConfirm = {
                                awsSdkViewModel.showSettingsDialog.value = false
                                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                intent.data = Uri.fromParts("package", packageName, null)
                                startActivity(intent)
                            },
                            onDismissRequest = { },
                            onConfirmText = stringResource(id = R.string.settings),
                            onDismissText = stringResource(id = R.string.exit)
                        )
                    }
                }
            }
        }
    }

    private fun handlePermissionResult(isGranted: Boolean) {
        if (isGranted) {
            startNavigation.value = true
        } else {
            if (countState.value < 2) {
                permissionLauncher.launch(Manifest.permission.CAMERA)
                countState.value += 1
            } else {
                awsSdkViewModel.showSettingsDialog.value = true
            }
        }
    }
}