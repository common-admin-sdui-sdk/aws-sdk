package com.isu.awssdk.ui.screens

import android.content.Context
import android.util.Log
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import com.amplifyframework.core.Amplify
import com.amplifyframework.ui.liveness.ui.FaceLivenessDetector
import com.isu.awssdk.core.AwsSdkUtils.Companion.showToast
import com.isu.awssdk.core.NetworkConnection
import com.isu.awssdk.ui.AwsSdkViewModel

@Composable
fun SelfieCaptureScreen(
    navHostController: NavHostController,
    awsSdkViewModel: AwsSdkViewModel
) {
    val context = LocalContext.current
    val startFaceDetectLiveliness = remember {
        mutableStateOf(false)
    }

    if (NetworkConnection.isNetworkAvailable(context = context)) {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = MaterialTheme.colorScheme.surface
        ) {
            FaceLivenessDetector(
                sessionId = awsSdkViewModel.sessionId.value,
                region = "ap-south-1",
                disableStartView = true,
                onComplete = {
                    Log.e("Face Liveness Status", "SelfieCaptureScreen: Face Liveness Completed", )
                    awsSdkViewModel.startFaceLivelinessSession(
                        context = context,
                        navHostController = navHostController
                    )
                },
                onError = { error ->
                    Log.e("Face Liveness Status", "SelfieCaptureScreen: Face Liveness Error", )
                    context.showToast(msg = error.message)
                }
            )
        }
    } else {
        awsSdkViewModel.showInternetDialog.value = true
    }
}

//    if (startFaceDetectLiveliness.value) {
//        SelfieCaptureContent(
//            context = context,
//            awsSdkViewModel = awsSdkViewModel,
//            navHostController = navHostController
//        )
//    }
//
//    if (awsSdkViewModel.showLoader.value) {
//        CustomLoader()
//    }
//
//    if (awsSdkViewModel.showInternetDialog.value) {
//        CustomDialog(
//            warningDesc = stringResource(id = R.string.no_internet),
//            buttonText = stringResource(id = R.string.try_again),
//            imageVector = Icons.Default.SignalWifiConnectedNoInternet4,
//            setShowDialog = {
//                awsSdkViewModel.showInternetDialog.value = it
//            }
//        )
//    }
//}
//
//@Composable
//fun SelfieCaptureContent(
//    context: Context,
//    awsSdkViewModel: AwsSdkViewModel,
//    navHostController: NavHostController
//) {
//    Surface(
//        modifier = Modifier.fillMaxSize(),
//        color = MaterialTheme.colorScheme.surface
//    ) {
//        FaceLivenessDetector(
//            sessionId = awsSdkViewModel.sessionId.value,
//            region = "ap-south-1",
//            disableStartView = true,
//            onComplete = {
//                awsSdkViewModel.startFaceLivelinessSession(
//                    context = context,
//                    navHostController = navHostController
//                )
//            },
//            onError = { error ->
//                context.showToast(msg = error.message)
//            }
//        )
//    }
//}
//
@Composable
fun SignInContent(
    context: Context,
    userName: String,
    password: String,
    setFaceDetectLiveliness: (Boolean) -> Unit,
) {
    Amplify.Auth.signIn(userName, password, { result ->
        if (result.isSignedIn) {
            setFaceDetectLiveliness.invoke(true)
        } else {
            setFaceDetectLiveliness.invoke(false)
            context.showToast("Unable find to user mapped with AWS.")
        }
    }, {
        setFaceDetectLiveliness.invoke(false)
        context.showToast("exceptionAuth: ${it.localizedMessage}")
    }
    )
}