package com.isu.awssdk.ui.navgraph

import com.isu.awssdk.core.AwsSdkConstants

sealed class AwsSdkDestination(val route: String) {
    data object DocumentCapture : AwsSdkDestination(route = AwsSdkConstants.DOC_CAPTURE)
    data object Instruction : AwsSdkDestination(route = AwsSdkConstants.INSTRUCTION)
    data object DocumentReview : AwsSdkDestination(route = AwsSdkConstants.DOC_REVIEW)
    data object SelfieCapture : AwsSdkDestination(route = AwsSdkConstants.SELFIE_CAPTURE)
}