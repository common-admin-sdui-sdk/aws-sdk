package com.isu.awssdk.ui.screens

import android.content.Context
import android.util.Size
import android.widget.Toast
import androidx.camera.core.ImageCapture
import androidx.camera.view.CameraController
import androidx.camera.view.LifecycleCameraController
import androidx.camera.view.PreviewView
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PhotoCamera
import androidx.compose.material.icons.filled.SignalWifiConnectedNoInternet4
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavHostController
import com.isu.awssdk.R
import com.isu.awssdk.core.AwsCamerax
import com.isu.awssdk.core.AwsSdkConstants
import com.isu.awssdk.core.AwsSdkUtils.Companion.imageDescription
import com.isu.awssdk.core.NetworkConnection
import com.isu.awssdk.ui.AwsSdkViewModel
import com.isu.awssdk.ui.composables.CustomDialog
import com.isu.awssdk.ui.composables.CustomImageDesc
import com.isu.awssdk.ui.composables.CustomTitle
import com.isu.awssdk.ui.navgraph.AwsSdkDestination

@Composable
fun DocumentCaptureScreen(navController: NavHostController, awsSdkViewModel: AwsSdkViewModel) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current

    if (NetworkConnection.isNetworkAvailable(context = context)) {
        DocumentCaptureContent(
            navHostController = navController,
            context = context,
            lifecycleOwner = lifecycleOwner,
            awsSdkViewModel = awsSdkViewModel
        )
    } else {
        awsSdkViewModel.showInternetDialog.value = true
    }

    if (awsSdkViewModel.showInternetDialog.value) {
        CustomDialog(
            warningDesc = stringResource(id = R.string.no_internet),
            buttonText = stringResource(id = R.string.try_again),
            imageVector = Icons.Default.SignalWifiConnectedNoInternet4,
            setShowDialog = {
                awsSdkViewModel.showInternetDialog.value = it
            }
        )
    }
}

@Composable
fun DocumentCaptureContent(
    navHostController: NavHostController,
    context: Context,
    lifecycleOwner: LifecycleOwner,
    awsSdkViewModel: AwsSdkViewModel
) {
    val controller = remember {
        LifecycleCameraController(context).apply {
            setEnabledUseCases(
                CameraController.IMAGE_CAPTURE
            )
            imageCaptureMode = ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY
            imageCaptureTargetSize = CameraController.OutputSize(Size(640, 480))
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(20.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        CustomTitle(title = stringResource(id = R.string.capture_doc))

        Text(
            modifier = Modifier.padding(top = 25.dp),
            text = "Place your document correctly within the frame.",
            style = MaterialTheme.typography.bodySmall,
            color = MaterialTheme.colorScheme.primary,
            textAlign = TextAlign.Center
        )

        CustomImageDesc(imageName = AwsSdkConstants.INTENT_IMAGE_TYPE.imageDescription())

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight() // Adjust the height percentage as needed
                .clip(shape = RoundedCornerShape(percent = 5)),
            propagateMinConstraints = true,
            contentAlignment = Alignment.Center
        ) {
            CameraPreview(
                lifecycleOwner = lifecycleOwner,
                controller = controller
            )
        }

        // Move the IconButton outside the Box
        Spacer(modifier = Modifier.weight(1f)) // Spacer to push IconButton to the bottom

        IconButton(
            modifier = Modifier.padding(bottom = 30.dp)
                .background(color = MaterialTheme.colorScheme.primary, shape = CircleShape)
                .size(70.dp),
            onClick = {
                AwsCamerax.captureDocument(
                    context = context,
                    controller = controller,
                    onImageCaptured = {
                        awsSdkViewModel.storeCardImage(image = it)
                        navHostController.navigate(route = AwsSdkDestination.DocumentReview.route)
                    }
                ) {
                    Toast.makeText(context, it.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            }
        ) {
            Icon(
                imageVector = Icons.Default.PhotoCamera,
                contentDescription = "",
                tint = MaterialTheme.colorScheme.onPrimary
            )
        }
    }

}

@Composable
fun CameraPreview(
    lifecycleOwner: LifecycleOwner,
    controller: LifecycleCameraController
) {
    AndroidView(
        modifier = Modifier
            .fillMaxWidth()
            .height(220.dp),
        factory = { context ->
            PreviewView(context).apply {
                this.controller = controller
                this.scaleType = PreviewView.ScaleType.FILL_CENTER
                this.implementationMode = PreviewView.ImplementationMode.COMPATIBLE
                controller.bindToLifecycle(lifecycleOwner)
            }
        }
    )
}