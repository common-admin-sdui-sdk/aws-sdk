package com.isu.awssdk.ui.screens

import android.content.Context
import android.net.Uri
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SignalWifiConnectedNoInternet4
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.isu.awssdk.R
import com.isu.awssdk.core.AwsSdkConstants
import com.isu.awssdk.core.NetworkConnection
import com.isu.awssdk.ui.AwsSdkViewModel
import com.isu.awssdk.ui.composables.CustomButton
import com.isu.awssdk.ui.composables.CustomDialog
import com.isu.awssdk.ui.composables.CustomLoader
import com.isu.awssdk.ui.composables.CustomOutlinedButton
import com.isu.awssdk.ui.composables.CustomTitle
import com.isu.awssdk.ui.navgraph.AwsSdkDestination

@Composable
fun DocumentReviewScreen(
    navHostController: NavHostController,
    awsSdkViewModel: AwsSdkViewModel
) {
    val context = LocalContext.current

    LaunchedEffect(key1 = true) {
        awsSdkViewModel.getCardImage()
    }

    if (NetworkConnection.isNetworkAvailable(context = context)) {
        DocumentReviewContent(
            navHostController = navHostController,
            awsSdkViewModel = awsSdkViewModel,
            context = context
        )
    } else {
        awsSdkViewModel.showInternetDialog.value = true
    }

    if (awsSdkViewModel.showInternetDialog.value) {
        CustomDialog(
            warningDesc = stringResource(id = R.string.no_internet),
            buttonText = stringResource(id = R.string.try_again),
            imageVector = Icons.Default.SignalWifiConnectedNoInternet4,
            setShowDialog = {
                awsSdkViewModel.showInternetDialog.value = it
            }
        )
    }

    if (awsSdkViewModel.showLoader.value) {
        CustomLoader()
    }

    BackHandler {
        navHostController.popBackStack()
    }
}

@Composable
fun DocumentReviewContent(
    navHostController: NavHostController,
    awsSdkViewModel: AwsSdkViewModel,
    context: Context
) {

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(30.dp),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CustomTitle(title = stringResource(id = R.string.review_your_doc))

        Spacer(modifier = Modifier.height(30.dp))

        Text(
            modifier = Modifier
                .wrapContentSize(),
            text = "Is your document perfectly visible without any glare and blur?",
            style = MaterialTheme.typography.bodySmall,
            color = MaterialTheme.colorScheme.primary,
            textAlign = TextAlign.Center
        )

        Spacer(modifier = Modifier.height(30.dp))

        Image(
            modifier = Modifier
                .fillMaxWidth()
                .clip(shape = RoundedCornerShape(percent = 5)),
            painter = rememberAsyncImagePainter(model = awsSdkViewModel.reviewImage.value),
            contentDescription = "",
            contentScale = ContentScale.FillWidth,
            alignment = Alignment.Center
        )

        Spacer(modifier = Modifier.weight(1f))

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 30.dp)
                .align(alignment = Alignment.End)
        ) {
            CustomOutlinedButton(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth(),
                buttonText = stringResource(id = R.string.retake),
                padding = 10.dp,
                contentColor = MaterialTheme.colorScheme.primary
            ) {
                awsSdkViewModel.reviewImage.value = Uri.EMPTY
                navHostController.navigate(route = AwsSdkDestination.DocumentCapture.route)
            }

            CustomButton(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxWidth(),
                buttonText = stringResource(id = R.string.use_this_image),
                padding = 10.dp
            ) {
                when {
                    AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.AADHAAR_CARD_FRONT, ignoreCase = false) -> {
                        awsSdkViewModel.startAadhaarFrontExtraction(context = context)
                    }

                    AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.AADHAAR_CARD_BACK, ignoreCase = false) -> {
                        awsSdkViewModel.startAadhaarBackExtraction(context = context)
                    }

                    AwsSdkConstants.INTENT_IMAGE_TYPE.equals(AwsSdkConstants.PAN_CARD, ignoreCase = false) -> {
                        awsSdkViewModel.startPanExtraction(context = context)
                    }
                }
            }
        }
    }
}