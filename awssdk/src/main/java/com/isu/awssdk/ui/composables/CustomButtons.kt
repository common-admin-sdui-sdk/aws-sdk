package com.isu.awssdk.ui.composables

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp


@Composable
fun CustomButton(
    modifier: Modifier,
    buttonText: String,
    padding: Dp = 30.dp,
    buttonHeight: Dp = 50.dp,
    enabled: Boolean = true,
    containerColor: Color = MaterialTheme.colorScheme.primary,
    contentColor: Color = Color.White,
    onClick: () -> Unit,
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()
    val floatScale by animateFloatAsState(
        if (isPressed) 0.80f else 1f,
        label = ""
    )

    Button(
        modifier = modifier
            .height(buttonHeight)
            .padding(horizontal = padding)
            .graphicsLayer {
                scaleX = floatScale
                scaleY = floatScale
            },
        onClick = onClick,
        enabled = enabled,
        interactionSource = interactionSource,
        colors = ButtonDefaults.buttonColors(
            containerColor = containerColor,
            contentColor = contentColor,
            disabledContainerColor = Color.DarkGray,
            disabledContentColor = Color.Black
        ),
        shape = RoundedCornerShape(10)
    ) {
        Text(
            modifier = Modifier,
            text = buttonText,
            style = MaterialTheme.typography.labelMedium,
            color = MaterialTheme.colorScheme.onPrimary,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun CustomOutlinedButton(
    modifier: Modifier,
    buttonText: String,
    padding: Dp = 30.dp,
    buttonHeight: Dp = 50.dp,
    enabled: Boolean = true,
    containerColor: Color = MaterialTheme.colorScheme.primary,
    contentColor: Color = Color.White,
    onClick: () -> Unit,
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()
    val floatScale by animateFloatAsState(
        if (isPressed) 0.85f else 1f,
        label = ""
    )

    OutlinedButton(
        modifier = modifier
            .height(buttonHeight)
            .padding(horizontal = padding)
            .graphicsLayer {
                scaleX = floatScale
                scaleY = floatScale
            },
        onClick = onClick,
        enabled = enabled,
        interactionSource = interactionSource,
        colors = ButtonDefaults.outlinedButtonColors(
            containerColor = Color.Transparent,
            contentColor = contentColor,
            disabledContainerColor = Color.Transparent,
            disabledContentColor = contentColor.copy(alpha = 0.5f)
        ),
        shape = RoundedCornerShape(10),
        border = BorderStroke(width = 2.dp, color = containerColor)
    ) {
        Text(
            modifier = Modifier,
            text = buttonText,
            style = MaterialTheme.typography.labelMedium,
            color = contentColor,
            textAlign = TextAlign.Center
        )
    }
}