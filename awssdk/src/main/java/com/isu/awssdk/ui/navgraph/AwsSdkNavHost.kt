package com.isu.awssdk.ui.navgraph

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.core.EaseInElastic
import androidx.compose.animation.core.EaseInOutElastic
import androidx.compose.animation.core.EaseOutElastic
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.isu.awssdk.ui.AwsSdkViewModel
import com.isu.awssdk.ui.screens.DocumentCaptureScreen
import com.isu.awssdk.ui.screens.InstructionScreen
import com.isu.awssdk.ui.screens.DocumentReviewScreen
import com.isu.awssdk.ui.screens.SelfieCaptureScreen

@Composable
fun AwsSdkNavHost(
    navHostController: NavHostController,
    awsSdkViewModel: AwsSdkViewModel
) {
    NavHost(
        modifier = Modifier.fillMaxSize(),
        navController = navHostController,
        startDestination = AwsSdkDestination.Instruction.route
    ) {
        composable(
            route = AwsSdkDestination.Instruction.route,
            enterTransition = {
                enterAnimation(transitionScope = this)
            },
            exitTransition = {
                exitAnimation(transitionScope = this)
            }
        ) {
            InstructionScreen(
                navHostController = navHostController,
                awsSdkViewModel = awsSdkViewModel
            )
        }

        composable(
            route = AwsSdkDestination.DocumentCapture.route,
            enterTransition = {
                enterAnimation(transitionScope = this)
            },
            exitTransition = {
                exitAnimation(transitionScope = this)
            }
        ) {
            DocumentCaptureScreen(
                navController = navHostController,
                awsSdkViewModel = awsSdkViewModel
            )
        }

        composable(
            route = AwsSdkDestination.DocumentReview.route,
            enterTransition = {
                enterAnimation(transitionScope = this)
            },
            exitTransition = {
                exitAnimation(transitionScope = this)
            }
        ) {
            DocumentReviewScreen(
                navHostController = navHostController,
                awsSdkViewModel = awsSdkViewModel
            )
        }

        composable(
            route = AwsSdkDestination.SelfieCapture.route,
            enterTransition = {
                enterAnimation(transitionScope = this)
            },
            exitTransition = {
                exitAnimation(transitionScope = this)
            }
        ) {
            SelfieCaptureScreen(
                navHostController = navHostController,
                awsSdkViewModel = awsSdkViewModel
            )
        }
    }
}

fun exitAnimation(transitionScope: AnimatedContentTransitionScope<NavBackStackEntry>): ExitTransition {
    return transitionScope.slideOutOfContainer(
        towards = AnimatedContentTransitionScope.SlideDirection.Left,
        animationSpec = tween(200, easing = EaseOutElastic)
    )
}

fun enterAnimation(transitionScope: AnimatedContentTransitionScope<NavBackStackEntry>): EnterTransition {
    return transitionScope.slideIntoContainer(
        towards = AnimatedContentTransitionScope.SlideDirection.Right,
        animationSpec = tween(200, easing = EaseInElastic)
    )
}