package com.isu.awssdk.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.isu.awssdk.R

@Composable
fun CustomAlertDialog(
    message: String,
    shape: Shape = RoundedCornerShape(10.dp),
    onDismiss: () -> Unit,
    onConfirm: () -> Unit,
    onDismissRequest: () -> Unit,
    onConfirmText: String,
    onDismissText: String,
) {
    AlertDialog(
        modifier = Modifier,
        onDismissRequest = onDismissRequest,
        confirmButton = {
            TextButton(onClick = onConfirm) {
                Text(
                    modifier = Modifier,
                    text = onConfirmText,
                    style = MaterialTheme.typography.labelMedium,
                    color = MaterialTheme.colorScheme.onPrimary,
                    textAlign = TextAlign.Center
                )
            }
        },
        dismissButton = {
            TextButton(onClick = onDismiss) {
                Text(
                    modifier = Modifier,
                    text = onDismissText,
                    style = MaterialTheme.typography.labelMedium,
                    color = MaterialTheme.colorScheme.onPrimary,
                    textAlign = TextAlign.Center
                )
            }
        },
        icon = {

        },
        iconContentColor = MaterialTheme.colorScheme.primary,
        title = {
            Text(
                modifier = Modifier,
                text = stringResource(id = R.string.alert),
                style = MaterialTheme.typography.bodyLarge,
                color = MaterialTheme.colorScheme.onPrimary,
                textAlign = TextAlign.Start
            )
        },
        text = {
            Text(
                modifier = Modifier,
                text = message,
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.onPrimary,
                textAlign = TextAlign.Justify
            )
        },
        shape = shape,
        containerColor = Color.Transparent,
        titleContentColor = Color.Black,
        textContentColor = Color.Black,
        tonalElevation = 2.dp,
        properties = DialogProperties(
            dismissOnBackPress = false,
            dismissOnClickOutside = true
        )
    )
}

@Composable
fun CustomDialog(
    warningDesc: String,
    setShowDialog: (Boolean) -> Unit,
    buttonText: String,
    imageVector: ImageVector
) {
    Dialog(
        onDismissRequest = { setShowDialog(false) },
        properties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = false
        )
    ) {
        Surface(
            modifier = Modifier,
            shape = RoundedCornerShape(20.dp),
            tonalElevation = 10.dp,
            shadowElevation = 5.dp,
            color = MaterialTheme.colorScheme.surface
        ) {
            Column(
                modifier = Modifier.padding(30.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Image(
                    modifier = Modifier
                        .background(
                            shape = CircleShape,
                            color = MaterialTheme.colorScheme.primary
                        )
                        .size(50.dp)
                        .padding(10.dp),
                    imageVector = imageVector,
                    contentDescription = "",
                    colorFilter = ColorFilter.tint(MaterialTheme.colorScheme.onPrimary)
                )

                Spacer(modifier = Modifier.height(30.dp))

                Text(
                    modifier = Modifier,
                    text = warningDesc,
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.onSurface,
                    textAlign = TextAlign.Center
                )

                Spacer(modifier = Modifier.height(30.dp))

                CustomButton(
                    modifier = Modifier.fillMaxWidth(),
                    buttonText = buttonText,
                    buttonHeight = 40.dp
                ) {
                    setShowDialog(false)
                }
            }
        }
    }
}


@Composable
fun CustomTitle(title: String) {
    Text(
        modifier = Modifier.padding(top = 30.dp),
        text = title,
        style = MaterialTheme.typography.titleLarge,
        color = MaterialTheme.colorScheme.primary,
        textAlign = TextAlign.Center
    )
}

@Composable
fun CustomImageDesc(imageName: String) {
    Text(
        modifier = Modifier.padding(top = 25.dp),
        text = imageName,
        style = MaterialTheme.typography.bodyMedium,
        color = MaterialTheme.colorScheme.primary,
        textAlign = TextAlign.Center
    )
}